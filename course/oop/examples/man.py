class Point:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z

    def __repr__(self):
        return f"<x={self.x} y={self.y} z={self.z}>"


class Man:
    def __init__(self, name, age, point):
        self.name = name
        self.age = age
        self.point = point  # Композиция

    def __repr__(self):
        return f"<name={self.name} age={self.age} point={repr(self.point)}>"

    def goto(self, point):
        self.point = point


class Welder(Man):  # Наследование
    def __init__(self, name, age, point, rank, salary):
        super().__init__(name, age, point)

        self.rank = rank
        self.salary = salary

    def __repr__(self):
        return f"<man={super().__repr__()} rank={self.rank} salary={self.salary}>"


andrey = Welder("Андрей", 20, Point(0, 0, 0), 3, 40_000)
print(andrey)
