class Size:
    def __init__(self, length, height, width):
        self.length = length
        self.height = height
        self.width = width

    def calculate_volume(self):
        return self.length * self.width * self.height


class Room:
    def __init__(self, wall_color, size):
        self.wall_color = wall_color
        self.size = size


room = Room("Yellow", Size(7, 10, 15))

print(room.wall_color, room.size.calculate_volume())
