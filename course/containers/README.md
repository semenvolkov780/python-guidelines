# Структуры данных (контейнеры)

## Index

[Введение](./introduction.md)

[Строка](./str.md)

[Список](./list.md)

[Кортеж](./tuple.md)

[Словарь](./dict.md)

[Множество](./set.md)

[Байты](./bytes.md)

## TODO

- collections
  - defaultdict
  - Counter
  - deque
  - OrderedDict

- Custom structures
  - collections.namedtuple
  - typing.NamedTuple
  - dataclasses.dataclass
  - types.SimpleNamespace
  
- reversed/sorted
