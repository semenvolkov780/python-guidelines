# Исключения

Мы не раз "ловили ошибку" во время разработки программ. Однако ошибки,
называемые в питоне исключениями, часто могут быть обработаны.

Начать понимание следует с того, что любое исключение в Python — это класс.

## Иерархия исключений

Существует ряд встроенных классов исключений, находящихся в иерархической
системе, то есть существуют общие классы исключений, а от них наследуются
более частные.

Позволю себе скопировать дерево исключений
[из документации](https://docs.python.org/3/library/exceptions.html#exception-hierarchy).

```
BaseException
 +-- SystemExit
 +-- KeyboardInterrupt
 +-- GeneratorExit
 +-- Exception
      +-- StopIteration
      +-- StopAsyncIteration
      +-- ArithmeticError
      |    +-- FloatingPointError
      |    +-- OverflowError
      |    +-- ZeroDivisionError
      +-- AssertionError
      +-- AttributeError
      +-- BufferError
      +-- EOFError
      +-- ImportError
      |    +-- ModuleNotFoundError
      +-- LookupError
      |    +-- IndexError
      |    +-- KeyError
      +-- MemoryError
      +-- NameError
      |    +-- UnboundLocalError
      +-- OSError
      |    +-- BlockingIOError
      |    +-- ChildProcessError
      |    +-- ConnectionError
      |    |    +-- BrokenPipeError
      |    |    +-- ConnectionAbortedError
      |    |    +-- ConnectionRefusedError
      |    |    +-- ConnectionResetError
      |    +-- FileExistsError
      |    +-- FileNotFoundError
      |    +-- InterruptedError
      |    +-- IsADirectoryError
      |    +-- NotADirectoryError
      |    +-- PermissionError
      |    +-- ProcessLookupError
      |    +-- TimeoutError
      +-- ReferenceError
      +-- RuntimeError
      |    +-- NotImplementedError
      |    +-- RecursionError
      +-- SyntaxError
      |    +-- IndentationError
      |         +-- TabError
      +-- SystemError
      +-- TypeError
      +-- ValueError
      |    +-- UnicodeError
      |         +-- UnicodeDecodeError
      |         +-- UnicodeEncodeError
      |         +-- UnicodeTranslateError
      +-- Warning
           +-- DeprecationWarning
           +-- PendingDeprecationWarning
           +-- RuntimeWarning
           +-- SyntaxWarning
           +-- UserWarning
           +-- FutureWarning
           +-- ImportWarning
           +-- UnicodeWarning
           +-- BytesWarning
           +-- ResourceWarning
```

В этой статье же я ограничусь лишь несколькими примерами.

## Конструкция `try-[except and/or finally]`

Имеет такой вид:

```python
try:
    """
    Тут можно писать код, который потенциально
    может вызвать ошибку (исключение)
    """
except ...:  # Класс исключения или кортеж классов исключений
    """
    Если произошло исключение того типа (одного из тех типов),
    управление передается сюда, программа не будет аварийно завершена.
    Если, конечно, в этом блоке еще раз не возникнет исключение...
    """
finally:
    """
    Этот блок будет выполнен в любом случае! Была ошибка или нет — не
    имеет никакого значения.
    """
```

Блок `try` является обязательным. За ним должен следовать как минимум один из
блоков `except` или `finally` или стразу оба. Но блок `finally` обязан
находиться в конце. Блоков `except` может быть несколько.

Пусть мы делаем калькулятор (который для простоты умеет только делить):

```python
print(float(input()) / float(input()))
```

Очевидно, что он сломается, если кто-то вторым числом введет 0.

```
Traceback (most recent call last):
  File "...", line 1, in <module>
    print(float(input()) / float(input()))
ZeroDivisionError: float division by zero
```

А давайте учтем это исключение!

```python
try:
    print(float(input()) / float(input()))
except ZeroDivisionError:
    print("А вот на 0 делить нельзя. Не знали?")
```

```
А вот на 0 делить нельзя. Не знали?
```

Ну вот теперь-то все ок! Хотя я забыл пару раз, что числа нужно вводить
по одному в строке, а не через пробел, и программа падала:

```
Traceback (most recent call last):
  File "...", line 2, in <module>
    print(float(input()) / float(input()))
ValueError: could not convert string to float: '1 0'
```

Давайте и это учтем:

```python
try:
    print(float(input()) / float(input()))
except ZeroDivisionError:
    print("А вот на 0 делить нельзя. Не знали?")
except ValueError:
    print("Так-так-так... числа вводим по одному в строке!")
```

Ок.

## Единый обработчик нескольких исключений

Можно написать, если не хочется обрабатывать все исключения по отдельности:

```python
try:
    print(float(input()) / float(input()))
except (ZeroDivisionError, ValueError):
    print("Что-то явно пошло не так, но мы это обработали")
```

Просто передаем исключения кортежем. 

Если исключения из одного семейства, можно перехватывать их ближайшего
родителя. Для `ValueError` и `ZeroDivisionError` это `Exception`. Если мы
напишем `except Exception`, мы будем перехватывать обе эти ошибки, но и кучу
других! А это плохо. Не рекомендуется перехватывать столь общие исключения,
потому что мы можем "замолчать" что-то серьезное, и это приведет к багам.

А вот если мы получаем список или словарь и должны получить из него элемент
по ключу/индексу, например, `0`, то тут уже имеет смысл перехватывать
`LookupError`, а не `IndexError` и `KeyError` по отдельности. Но я бы не
сказал, что так делают регулярно.

```python
def foo(structure: list | dict, default=None):
    try:
        return structure[0]
    except LookupError:
        return default


def bar(structure: list | dict, default=None):
    try:
        return structure[0]
    except (IndexError, KeyError):
        return default


good_list = [1, 2, 3]
good_dict = {0: 1, 1: 2}
bad_list = []
bad_dict = {}

print(foo(good_list))
print(foo(good_dict))
print(foo(bad_list))
print(foo(bad_list))
print(bar(good_list))
print(bar(good_dict))
print(bar(bad_list))
print(bar(bad_list))
```

Функции `foo` и `bar` эквивалентны.

## Экземпляр класса исключения

Как правило, у объекта исключения есть свойство, в котором уточняется
дополнительная информация. Или же эту информацию нам предоставляет `__repr__`.
Получить экземпляр исключения можно через `as`.

```python
try:
    print(float(input()) / float(input()))
except (ZeroDivisionError, ValueError) as e:
    print(e)
```

```
could not convert string to float: '1 0'
```

...

```
float division by zero
```

## Ключевое слово `raise`

Используется, чтобы возбудить исключение.

```python
try:
    raise MemoryError("Test Memory Error")
except Exception as e:
    print(e)
```

```
Test Memory Error
```

После `raise` можно передать не экземпляр класса, а сам класс, тогда в
конструктор класса ничего не будет передано. Для встроенных исключений это
означает, что сообщение будет пустым.

Если вы хотите возбудить исключение в блоке `except`, то можно, конечно,
сделать это просто через `raise`. Но тогда это будет выглядеть как ошибка
во время обработки ошибки. Чтобы показать, что возбуждаемая ошибка получилась
из-за ошибки, которую вы обрабатываете, имеет смысл использовать `raise-from`.

```python
try:
    raise MemoryError("Test Memory Error")
except Exception as e:
    raise RuntimeError from e
```

```
Traceback (most recent call last):
  File "...", line 2, in <module>
    raise MemoryError("Test Memory Error")
MemoryError: Test Memory Error

The above exception was the direct cause of the following exception:

Traceback (most recent call last):
  File "...", line 4, in <module>
    raise RuntimeError from e
RuntimeError
```

## Свои исключения

Просто отнаследуйтесь от уже существующего класса!

## Особенности некоторых исключений

Да, исключения достаточно часто можно понимать как ошибки, но в Python через
исключения происходит много всего интересного, что не всегда можно назвать
ошибкой.

Как мы уже помним, исключение `StopIteration` используется, чтобы показать,
что итерация завершена. И цикл `for` это исключение умеет обрабатывать.

Чтобы завершить программу, возбудите исключение `SystemExit`. Программа
завершится из-за исключения, но без ошибки.

Я уверен, вы много раз встречали ошибку `SyntaxError`. Давайте ее получим
"естественным" образом и попытаемся обработать:

```python
try:
    if = 2
except SyntaxError:
    print("No Syntax Error")
```

Ничего не вышло, так как из-за синтаксической ошибки программа даже
не запустилась.
