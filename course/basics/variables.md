# Переменные

## Проблема

Пусть задача такая: у v01d'а есть 2000 рублей. Пакет молока стоит 44 рубля.
Сколько пакетов молока сможет купить v01d и какова будет сдача?

Очевидно, что вычислить это можно так:

```python
print(2000 // 44, 2000 % 44)
```

```
45 20
```

Но что если мы не знаем заранее ни количество денег у v01d'а, ни стоимость
одного пакета молока? Как написать такую программу? Правильно, ввести данные
с клавиатуры после запуска программы.

```python
print(int(input()) // int(input()), int(input()) % int(input()))
```

Ввод

```
2000
44
2000
44
```

Вывод

```
45 20
```

Ок, это... ну, можно использовать, но мы заставляем пользователя вводить два
раза то, что он мог бы ввести один раз.

Нам нужен способ запомнить пользовательский ввод. И тут на помощь приходят
**переменные**.

## Создание и применение

Будем пока считать, что переменная — это такая коробочка, в которую можно что-то
положить, а потом, если будет нужно, посмотреть, что там.

У переменной есть имя. Оно может быть почти любым:

- Не может начинаться с цифры
- Не может содержать пробелов
- Не может содержать знаков пунктуации
- Может содержать `_`

А должно оно быть таким, чтобы было ясно, что в переменной лежит.

Создать переменную можно так:

```python
var_name = "Some value"
```

Была создана переменная `var_name`, в которую сохранена строка (`str`)
`"Some value"`.

Значение переменной — это те данные, которые в ней лежат.

Чтобы получить значение переменной, достаточно написать ее имя.

```python
a = 10
b = 20

print(a + b)  # 30
```

## Стиль

В Python принято называть переменные с маленькой буквы. Если имя длиннее одного
слова, разделять слова нужно знаком `_`.

```
very_good_name
badName
VeryBadName
```

## Типизация

Типизация (то есть система типов данных) в Python — **строгая (сильная)
динамическая**.

Динамическая, потому что, когда вы создаете переменную, вы не
обязаны сразу писать, какой тип данных в ней будет лежать.

Строгая, потому что
тип данных значения переменной не поменяется неявно. Например, в ЯП JavaScript
тоже динамическая, но уже слабая типизация. Если в JS сложить число со строкой,
то число будет неявно приведено к строке, и оператор `+` выполнит
конкатенацию (склеивание) двух строк: `123 + "KaveN"` будет вычислено как
`"123KaveN"`. Такое поведение может привести — и регулярно приводит — к багам.
Попробуйте проделать это в Python — программа завершится с ошибкой.

Как мне представляется, сильная динамическая типизация — это очень разумный
компромисс между гибкостью, скоростью разработки и надежностью программ.

Сегодня опытные Python-программисты стараются указывать типы данных переменных.
Это нужно, чтобы их инструменты разработки могли указывать на ошибку, если
где-то тип данных не совпадет с тем, который должен быть. Однако работает эта
проверка только до запуска программы. Если во время выполнения (runtime) в
какой-то переменной окажется не тот тип данных, Python на это никак не
отреагирует.

Синтаксис такой: пишем название переменной, двоеточие, пробел, тип данных.
Кстати, тип можно указать и без присваивания (`=`), но в примере оно будет:

```python
age: int = 20
name: str = "v01d"

print(f"Hello! My name is {name}. I'm {age} y.o.")
```

Тут все ок:
![OK](https://sun9-17.userapi.com/impg/wbUymIOhNdouCQHuuV7RskHjxj-fRhB2KJC2Cw/gJsFEPefS08.jpg?size=700x152&quality=96&sign=9776b1006d884bfb672935ebb9171c53&type=album)

А вот тут PyCharm заметил, что я записываю в переменную значение не того типа
данных, что указал. Но так как типизация динамическая, работать все будет без
ошибок:

![Not OK](https://sun9-39.userapi.com/impg/E5YGIz95qxVfdeYv-l3nGIXJiQQT4_2ucTkEnw/0KscUABFEZ8.jpg?size=702x153&quality=96&sign=e4e579a6c70d64c57be7060f54317631&type=album)

На этом этапе от вас не требуется указывать тип данных, но когда дело дойдет
до более сложных программ, это может хорошо помочь вам.

## Константы

Константа — неизменяемая переменная. 

В Python нет возможности запретить изменять значение переменной.

Однако если нужно создать такие переменные, которые не следует менять, называть
их нужно большими буквами. Слова все еще разделяются символом `_`.

```
MATH_PI = 3.14
FPS_MAX = 60
API_TOKEN = "Q4KzqC5y4Rjdk4KPyFmpJYysYUnJWOLc"
```

Такие переменные очень условно называют константами.

Создавать их следует вверху файла, до основного кода.
