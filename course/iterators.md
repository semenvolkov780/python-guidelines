# Итераторы

Что такое итерируемый объект — вы знаете. А вот управляет процессом итерации
итератор.

Итератор — инструмент управления тем, как перебрать все элементы
последовательности.

Кстати, итерироваться можно не только по реально существующим элементам
последовательности, но и создавать их на лету!

## Как использовать итератор

До этого мы использовали цикл `for` как менеджер итераторов. Сейчас посмотрим,
что же за магия в нем скрыта.

Пусть есть итерируемый объект `o`. Например, `list`.

```python
o = [1, 2, 3, 4, 5]
```

Как последовательно получить все его элементы без цикла `for`? Для начала
нужно взять итератор от итерируемого объекта. Сделать это, вызвав
функцию `iter`.

```python
o = [1, 2, 3, 4, 5]
it = iter(o)
print(it)
```

```
<list_iterator object at 0x000001AEB2F64AF0>
```

Теперь можно вызывать функцию `next` от этого итератора. И каждый раз она
будет возвращать очередной элемент списка.

```python
o = [1, 2, 3, 4, 5]
it = iter(o)

print(next(it))
print(next(it))
print(next(it))
print(next(it))
print(next(it))
```

```
1
2
3
4
5
```

Вот мы и перебрали последовательность без цикла `for`! Но как `for` понимает,
что пора остановиться? Если вызвать `next(it)` еще раз, получим ошибку
`StopIteration`:

```
Traceback (most recent call last):
  File "...", line ..., in <module>
    print(next(it))
StopIteration
```

Цикл `for` умеет эту ошибку перехватывать и обрабатывать. Но об этом в другой
раз.

У функций `iter` и `next` есть дополнительные аргументы, которые могут
изменить поведение данных функций. Чтобы глубже изучить их работу, прочитайте
[это](https://docs.python.org/3/library/functions.html#iter)
и [это](https://docs.python.org/3/library/functions.html#next).

## Делаем итерируемыми свои классы

### Как оно есть

Чтобы объект был **итерируемым**, нужно реализовать магический метод `__iter__`,
который будет возвращать итератор. Именно этот метод и будет
вызван функцией `iter`!

А как сделать итератор? Чтобы объект был **итератором**, нужно реализовать метод
`__next__`, который будет возвращать очередное значение. Тааааак, держитесь ;-):

```python
class Iterator:
    def __init__(self, start=0, end=10):
        self.current = start
        self.end = end

    def __next__(self):
        current = self.current

        if current == self.end:  # Мб, хватит уже?
            raise StopIteration  # Ошибка возбуждается так, придется поверить

        self.current += 1

        return current


class Iterable:
    def __iter__(self):
        return Iterator()


iterable = Iterable()

for x in iterable:
    print(x, end=" ")
```

Единственное, что вы пока можете не знать, — ключевое слово `raise`,
использованное мной для возбуждения исключения `StopIteration`, которое, как
мы помним, сигнализирует об окончании итерации. Про ошибки (или исключения, как
их называют в питоне) будем говорить потом. Остальное постарайтесь понять...

Пример выше мог показаться вам сложным, избыточным, многословным. И вы все
еще думаете, что Python — это простой язык? Язык для детей?

Но все же вспомним, что Python стремится к простоте. Я покажу более простой
способ, но поймите, что пример выше необходим, чтобы вы понимали, как все
работает на самом деле, без "синтаксического сахара". Если поняли,
читайте дальше.

### Генераторы

Генератор — это вызываемый объект (функция), который возвращает итератор.

Метод `Iterable.__iter__` был как раз генератором. Существует хороший способ
создать генератор без создания отдельного класса-итератора.

Создаем обычную функцию, но вместо `return` используем ключевое слово `yield`.
Оно тоже позволяет вернуть значение, но не останавливает выполнение функции.

```python
def generate(start, end, step=1):
    current = start
    while current < end:
        yield current
        current += step


it = generate(2, 8)

print(next(it))
print(next(it))

for x in it:
    print(x, end=" ")
```

```
2
3
4 5 6 7
```

Функция `generate` возвращает итератор. Когда вызывают `next(it)`, выполняется
описанное тело функции, пока не встретится `yield`. На этом выполнение пока
останавливается, `next` возвращает то, что мы вернули через `yield`.

При следующем вызове `next(it)` исполнение продолжается с той точки, где мы
остановились в прошлый раз.

Когда цикл завершается, возникает исключение `StopIteration`.

Как вы уже могли заметить, циклу `for` можно скормить не только итерируемый
объект, но начатый ("откусанный") итератор.

Перепишем наш класс:

```python
class Iterable:
    def __iter__(self):
        i = 0
        while i < 10:
            yield i
            i += 1


iterable = Iterable()

for x in iterable:
    print(x, end=" ")  # 0 1 2 3 4 5 6 7 8 9
```

Результат не поменялся.

## Генераторные выражения (Generator Expressions)

Выражение вида `(expr for x in iterable [if cond])` называется
генераторным выражением. В конце может быть условие, зависящее от `x`, при
котором генератор вернет `expr`, но его можно и не писать.

Пример:

```python
iterator1 = (x ** 2 for x in range(7))
iterator2 = (x + 1000 for x in range(7) if x % 2)

print(*iterator1)
print(*iterator2)
```

```
0 1 4 9 16 25 36
1001 1003 1005
```

Значением генераторного выражения оказался итератор, который я распаковал
оператором `*`.

В некоторых случаях скобки у генераторного выражения можно не писать: например,
если оно передано как аргумент при вызове какой-то функции. Там ведь и так уже
будут скобки.

Напоминаю, что в итоге итератор "опустошается" на `StopIteration`, поэтому
перебрать его дважды нельзя.

На основе генераторного выражения в Python создали выражения, создающие
некоторые встроенные последовательности.

```python
print([letter for letter in "ErViNcArKaVeN" if letter.isupper()])
print({letter for letter in "ErViNcArKaVeN" if letter.islower()})
print({letter: ord(letter) for letter in "ErViNcArKaVeN"})
```

```
['E', 'V', 'N', 'A', 'K', 'V', 'N']
{'e', 'r', 'c', 'i', 'a'}
{'E': 69, 'r': 114, 'V': 86, 'i': 105, 'N': 78, 'c': 99, 'A': 65, 'K': 75, 'a': 97, 'e': 101}
```

Были созданы: список, множество и словарь.

Кстати, такой синтаксис создания списков называется List Comprehension, а на
русский это обычно переводят как свертывание списков, списочное включение...
Переводчик Google говорит, что это "Понимание списка".

Короче, лучше используйте английскую терминологию в программировании.

## Заключение

Я рассказал далеко не все об итераторах и генераторах!

Почитайте самостоятельно про:

- `yield from`
- `generator.send`
- Coroutine

Если, конечно, хотите глубоко познать этот замечательный ЯП.
